"""
REFs: 
 - https://en.wikipedia.org/wiki/Otsu's_method
 - https://stackoverflow.com/questions/4270301/matplotlib-multiple-datasets-on-the-same-scatter-plot
 - https://stackoverflow.com/questions/42730650/how-do-i-plot-an-image-and-a-graph-side-by-side
 - https://docs.opencv.org/4.x/d7/d4d/tutorial_py_thresholding.html
 - https://pyimagesearch.com/2014/11/03/display-matplotlib-rgb-image/


Algorithm
1) Compute histogram and probabilities of each intensity level
2) Set up initial {\displaystyle \omega _{i}(0)}\omega _{i}(0) and {\displaystyle \mu _{i}(0)}\mu _{i}(0)
3) Step through all possible thresholds {\displaystyle t=1,\ldots }{\displaystyle t=1,\ldots } maximum intensity
  3.1) Update {\displaystyle \omega _{i}}\omega _{i} and {\displaystyle \mu _{i}}\mu _{i}
  3.2) Compute {\displaystyle \sigma _{b}^{2}(t)}\sigma _{b}^{2}(t)
4) Desired threshold corresponds to the maximum {\displaystyle \sigma _{b}^{2}(t)}\sigma _{b}^{2}(t)

function level = otsu(histogramCounts)
total = sum(histogramCounts); % total number of pixels in the image 
%% OTSU automatic thresholding
top = 256;
sumB = 0;
wB = 0;
maximum = 0.0;
sum1 = dot(0:top-1, histogramCounts);
for ii = 1:top
    wF = total - wB;
    if wB > 0 && wF > 0
        mF = (sum1 - sumB) / wF;
        val = wB * wF * ((sumB / wB) - mF) * ((sumB / wB) - mF);
        if ( val >= maximum )
            level = ii;
            maximum = val;
        end
    end
    wB = wB + histogramCounts(ii);
    sumB = sumB + (ii-1) * histogramCounts(ii);
end
end
"""
import numpy as np
from sys import argv

from cv2 import imread, cvtColor, COLOR_RGB2GRAY, threshold, THRESH_BINARY, COLOR_BGR2RGB
import matplotlib.pyplot as plt

def compute_otsu_criteria(image, threshold):
    # create the thresholded image
    thresholded_image = np.zeros(image.shape)
    thresholded_image[image >= threshold] = 1

    # compute weights
    nb_pixels = image.size
    nb_pixels1 = np.count_nonzero(thresholded_image)
    weight_1 = nb_pixels1 / nb_pixels
    weight_0 = 1 - weight_1

    # if one the classes is empty, eg all pixels are below or above the threshold, that threshold will not be considered
    # in the search for the best threshold
    if weight_1 == 0 or weight_0 == 0:
        return np.inf

    # find all pixels belonging to each class
    val_pixels_1 = image[thresholded_image == 1]
    val_pixels_0 = image[thresholded_image == 0]

    # compute variance of these classes
    var_0 = np.var(val_pixels_0) if len(val_pixels_0) > 0 else 0
    var_1 = np.var(val_pixels_1) if len(val_pixels_1) > 0 else 0

    return int(weight_0 * var_0 + weight_1 * var_1)

def get_Image(path=".", index=0):
    from os import walk
    filenames = [
        file for file in next(walk(path), (None, None, []))[2] if
        ".png" in file or
        ".jpeg" in file or
        ".jpg" in file
    ]
    print(filenames)
    return filenames[index] if len(filenames) > index else ""


if __name__ == '__main__':
    
    # load your image as a numpy array.
    # load your image as a numpy array.
    image = imread(argv[1]) if len(argv) > 1 else imread(get_Image())
    image = cvtColor(image, COLOR_BGR2RGB)
    # For testing purposes, one can use for example image = np.random.randint(0,255, size = (50,50))

    # testing all thresholds from 0 to the maximum of the image
    threshold_range = range(np.max(image) + 1)
    criterias = [compute_otsu_criteria(image, threshold) for threshold in threshold_range]

    # best threshold is the one minimizing the Otsu criteria
    best_threshold = threshold_range[np.argmin(criterias)]
    x_axis = [i for i in range(len(criterias))]

    print(f"criterias[{best_threshold}] ==", criterias[best_threshold])

    fig = plt.figure()
    ax1 = fig.add_subplot(221)
    plt.imshow(image)

    ax2 = fig.add_subplot(222)
    image_grayscale = cvtColor(image, COLOR_RGB2GRAY)
    ret, image_thresholded = threshold(
        image_grayscale, best_threshold, 255, THRESH_BINARY
    )
    plt.imshow(image_thresholded, cmap="gray")

    ax3 = fig.add_subplot(223)
    ax3.scatter(
        x_axis, criterias,
        s=3, c='b', marker="s", label='Otsu Criterias'
    )
    ax3.scatter(
        best_threshold, criterias[best_threshold],
        s=30, c='r', marker="o", label=f'Best Threshold: {best_threshold}'
    )
    plt.legend(loc='upper left')
    
    ax4 = fig.add_subplot(224)
    plt.hist(image_grayscale, 10)

    plt.show()
